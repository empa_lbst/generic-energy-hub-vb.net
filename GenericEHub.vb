﻿''' ********************Generic Energy Hub VB.NET
''' ***************Author: C.Waibel, October 2015
''' *********************************************
''' 
Imports ILOG.Concert
Imports ILOG.CPLEX
Imports Excel = Microsoft.Office.Interop.Excel

Public Class GenericEHub

    Public Overloads Sub Main(YourInputPath As String)
        Dim cpl As New Cplex()


        ' /*****************************************************************************
        ' ******************************************************************************
        ' INPUT PARAMETERS
        ' ******************************************************************************
        ' ******************************************************************************/
        Dim p As Integer    'period
        Dim t As Integer    'tech
        Dim u As Integer    'endUse '; 0 = electricity; 1 = heating


        '       //INDEX DECLARATIONS
        Dim Tech() As String = {"HP", "Boiler", "CHP", "PV", "ST"}
        Dim Period As Integer = 744     '1 hour periods
        Dim EndUse As Integer = 2       '0 = electricity; 1 = heating

        '       //WEATHER DATA
        Dim LoadSolarData As String = YourInputPath & "\Data.xls"
        Dim Solar As Double() = OpenExcelGetData(LoadSolarData, Period, "A")    'average solar radiation in each period (W/m2)

        '       //DEMAND DATA
        Dim LoadDemandData As String = YourInputPath & "\Demand.xls"
        Dim abc() As String = {"A", "B", "C", "D", "E", "F", "G", "H", "I"}
        Dim Demand(EndUse - 1)() As Double                                      'Demand
        For u = 0 To EndUse - 1                                                 'in ILOG IDE: float Demand[Period][EndUse] , here Demand(EndUse)(Period)
            Demand(u) = New Double(Period - 1) {}
            Demand(u) = OpenExcelGetData(LoadDemandData, Period, abc(u))
        Next


        '       //TECHNOLOGY DATA
        Dim MaxCap() As Double = {10, 50, 50, 1, 3.5}               'Maximum capacity (kW)
        Dim CapCost() As Double = {1000, 200, 1500, 3500, 2900}     'Capital cost (chf per kW)
        Dim OMFCost() As Double = {0, 0, 0, 0, 0}                   'Fixed O&M cost (chf per kWh)
        Dim OMVCost() As Double = {0.1, 0.01, 0.021, 0.06, 0.12}    'Variable O&M cost (chf per kWh)
        Dim Efficiency() As Double = {3.2, 0.94, 0.3, 0.14, 0.46}   'Generation efficiency (%)
        Dim Lifetime() As Double = {20, 30, 20, 20, 35}             'Unit lifetime (years)
        Dim htp() As Double = {1, 1, 1.73, 0, 1}                    'heat to power ratio
        Dim MinLoad() As Double = {0.0, 0.0, 0.5, 0.0, 0.0}         'Minimum acceptable load (%)
        Dim Appl() As Double = {2, 3, 5, 11, 11}                    'Grouping of technologies
        Dim CO2EF() As Double = {0, 0.198, 0.198, 0, 0}             'technology CO2 emission factor (based on fuel type)
        Dim Area() As Double = {0, 0, 0, 8, 5}                      'Area of solar panel

        '       //Battery Data
        Dim CostBat As Double = 1000                                'Battery capital cost
        Dim LifeBat As Double = 20                                  'Battery lifetime
        Dim ch_eff As Double = 0.9                                  'Battery charing efficiency
        Dim disch_eff As Double = 0.9                               'Battery discharging efficiency
        Dim decay As Double = 0.001                                 'Battery hourly decay
        Dim max_ch As Double = 0.3                                  'Battery max charging rate
        Dim max_disch As Double = 0.3                               'Battery max discharging rate
        Dim min_state As Double = 0.3                               'Battery minimum state of charge

        '       //Thermal Energy Storage Tank Data
        Dim CostTES As Double = 100                                 'TES capital cost (chf/kW)
        Dim LifeTES As Double = 17                                  'TES lifetime
        Dim ceff_TES As Double = 0.9                                'TES charging efficiency
        Dim deff_TES As Double = 0.9                                'TES discharging efficiency
        Dim heatloss As Double = 0.001                              'TES heat loss
        Dim maxch_TES As Double = 0.25                              'TES max charging rate
        Dim maxdisch_TES As Double = 0.25                           'TES max discharging rate

        '       //SOLAR DATA
        Dim MaxRoof As Double = 250                                 'Maximum available roof area
        Dim OtherModEff As Double = 0.89                            'Cabling efficiency (85-95%), inverter efficiency (95-99%), and temperature efficiency (90-95%)

        '       //ECONOMIC DATA
        Dim eprice As Double = 0.24                                 'electricity price
        Dim gprice As Double = 0.09                                 'gas price
        Dim esell As Double = 0.14                                  'Feed in tarrif
        Dim intrate As Double = 0.08                                'interest rate

        Dim Annuity(Tech.Length - 1) As Double
        For i As Integer = 0 To Tech.Length - 1
            Annuity(i) = intrate / (1 - (1 / ((1 + intrate) ^ (Lifetime(i)))))         'Annuity factor for each tehnology
        Next
        Dim AnnuityTES As Double = intrate / (1 - (1 / ((2 + intrate) ^ (LifeTES))))    'Annuity factor for the TES
        Dim AnnuityBat As Double = intrate / (1 - (1 / ((1 + intrate) ^ (LifeBat))))    'Annuity factor for the battery

        '       //EMISSIONS/ENVIRONMENTAL DATA
        Dim RefBoilerEff As Double = 0.94                   'Reference boiler efficiency
        Dim GridCO2EF As Double = 0.137                     'Grid CO2 Emission Factor
        Dim NGCO2EF As Double = 0.198                       'Natural Gas CO2 Emission Factor
        Dim CO2Limit As Double = 0.1                       'Target emissions reduction (%)
        Dim hdump As Double = 1                             'Heat dump limit (%)



        '       //MISC INPUT PARAMETERS
        'Ability of each technology to generate each end use. ()(0) == 1 -> electricity; ()(1) == 1 -> heat
        Dim Suit(Tech.Length - 1)() As Integer
        For i = 0 To Tech.Length - 1
            Suit(i) = New Integer(EndUse - 1) {}
        Next
        Suit(0) = {0, 1}
        Suit(1) = {0, 1}
        Suit(2) = {1, 1}
        Suit(3) = {1, 0}
        Suit(4) = {0, 1}
        Dim M1 As Double = 100000000                        'Arbitrary large number




        '/*****************************************************************************
        '******************************************************************************
        '       MODEL DECLARATIONS
        '******************************************************************************
        '******************************************************************************/


        '//DECISION VARIABLES
        Dim Unit As INumVar() = cpl.IntVarArray(Tech.Length, 0, Integer.MaxValue)   'Number of units of each discrete tehnology that are purchased
        Dim ElecPur As INumVar() = cpl.NumVarArray(Period, 0.0, System.Double.MaxValue) 'Electricity purchased from grid during each time period
        Dim ElecSell As INumVar() = cpl.NumVarArray(Period, 0.0, System.Double.MaxValue) 'Excess electricity sold back to the grid during every time period

        'Number of units operating in each time interval
        Dim Operate(Period - 1)() As INumVar
        'Gen[Period][Tech][EndUse]; //Energy generation by each technology durig each time period
        Dim Gen()()() As INumVar
        Gen = New INumVar(Period - 1)()() {}
        'Excess heat that is dumped by CHP
        Dim dump(Period - 1)() As INumVar
        For p = 0 To Period - 1
            Operate(p) = cpl.IntVarArray(Tech.Length, 0.0, Integer.MaxValue)
            dump(p) = cpl.NumVarArray(Tech.Length, 0.0, System.Double.MaxValue)
            Gen(p) = New INumVar(Tech.Length - 1)() {}
            For t = 0 To Tech.Length - 1
                Gen(p)(t) = New INumVar(EndUse - 1) {}
                For u = 0 To EndUse - 1
                    Gen(p)(t)(u) = cpl.NumVar(0.0, System.Double.MaxValue)
                Next u
            Next t
        Next p


        'Dim y1 As INumVar() = cpl.IntVarArray(Period, 0, 1)             'binary control variable
        Dim y1 As INumVar() = cpl.BoolVarArray(Period)


        Dim BatCap As INumVar = cpl.NumVar(0.0, System.Double.MaxValue) 'Battery capacity installed
        Dim Charge_Bat As INumVar() = cpl.NumVarArray(Period, 0.0, System.Double.MaxValue)  'Electricity used to charge the battery (kWh)
        Dim Discharge_Bat As INumVar() = cpl.NumVarArray(Period, 0.0, System.Double.MaxValue) 'Electricity discharged from the battery (kWh)
        Dim Stored_Bat As INumVar() = cpl.NumVarArray(Period, 0.0, System.Double.MaxValue) 'Electricity currently stored in the battery (kWh)

        Dim TESCap As INumVar = cpl.NumVar(0.0, System.Double.MaxValue) 'TES capacity installed
        Dim Charge_TES As INumVar() = cpl.NumVarArray(Period, 0.0, System.Double.MaxValue) 'Electricity used to charge the TES (kWh)
        Dim Discharge_TES As INumVar() = cpl.NumVarArray(Period, 0.0, System.Double.MaxValue) 'Electricity discharged from the TES (kWh)
        Dim Stored_TES As INumVar() = cpl.NumVarArray(Period, 0.0, System.Double.MaxValue) 'Electricity currently stored in the TES (kWh)

        Dim CO2Emissions As INumVar = cpl.NumVar(0.0, System.Double.MaxValue) 'Total CO2 emissions fot the optimal energy system over the entire time horizon (kgCO2)


        ' OBJECTIVE FUNCTION DECOMPOSED BY TYPE OF COST
        Dim CapCostTech As ILinearNumExpr = cpl.LinearNumExpr() 'Annual capital cost of technologies
        Dim OMFCostTotal As ILinearNumExpr = cpl.LinearNumExpr()    'Fixed O&M cost of technologies

        For t = 0 To Tech.Length - 1
            CapCostTech.AddTerm((MaxCap(t) * Annuity(t) * CapCost(t)), Unit(t))
            OMFCostTotal.AddTerm((MaxCap(t) * OMFCost(t)), Unit(t))
        Next

        Dim CapCostTES As INumExpr = cpl.Prod((AnnuityTES * CostTES), TESCap)  'Annual capital cost of the TES 
        Dim CapCostBattery As INumExpr = cpl.Prod((AnnuityBat * CostBat), BatCap)  'Annual capital cost of the battery 

        Dim OMVCostTotal As ILinearNumExpr = cpl.LinearNumExpr()    'Variable O&M cost of technologies
        Dim NGCost1 As ILinearNumExpr = cpl.LinearNumExpr()         'Cost of natural gas consumed by CHP
        Dim NGCost2 As ILinearNumExpr = cpl.LinearNumExpr()         'Cost of natural gas consumed by boiler
        Dim ElecCost As ILinearNumExpr = cpl.LinearNumExpr()        'Cost of electricty purchased
        Dim ElecSold As ILinearNumExpr = cpl.LinearNumExpr()        'Cost of electricty sold back to grid
        For p = 0 To Period - 1
            For t = 0 To Tech.Length - 1
                For u = 0 To EndUse - 1
                    OMVCostTotal.AddTerm(OMVCost(t), Gen(p)(t)(u))
                    If Appl(t) = 5 And u = 0 Then
                        NGCost1.AddTerm((gprice / Efficiency(t)), Gen(p)(t)(u))
                    ElseIf Appl(t) = 3 And u = 1 Then
                        NGCost2.AddTerm((gprice / Efficiency(t)), Gen(p)(t)(u))
                    End If
                Next
            Next
            ElecCost.AddTerm(eprice, ElecPur(p))
            ElecSold.AddTerm(esell, ElecSell(p))
        Next



        '       /******************************************************************************
        '******************************************************************************  
        '       MODEL()
        '******************************************************************************
        '******************************************************************************/

        '//OBJECTIVE FUNCTION
        cpl.AddMinimize(cpl.Diff(cpl.Sum(CapCostTech, CapCostTES, CapCostBattery, _
                                  OMFCostTotal, OMVCostTotal, _
                                  NGCost1, NGCost2, ElecCost), ElecSold))

        '//CONSTRAINTS
        '  //ENERGY BALANCE CONSTRAINTS 
        For p = 0 To Period - 1
            '  //Electricity generated plus electricity purchased must be greater than or equal electricity demand
            Dim elecgenExpr As ILinearNumExpr = cpl.LinearNumExpr()
            Dim elecdemandExpr As ILinearNumExpr = cpl.LinearNumExpr()
            For t = 0 To Tech.Length - 1
                elecgenExpr.AddTerm(1, Gen(p)(t)(0))
                If Tech(t) = "HP" Then
                    elecdemandExpr.AddTerm((1 / Efficiency(t)), Gen(p)(t)(1))
                End If

                '  //OPERATION CONSTRAINTS
                'Number of units operating in each time interval has to be less than or equal to the number of units that were purchased
                cpl.AddLe(Operate(p)(t), Unit(t))
            Next
            elecgenExpr.AddTerm(1, ElecPur(p))
            elecgenExpr.AddTerm(disch_eff, Discharge_Bat(p))
            elecdemandExpr.AddTerm((1 / ch_eff), Charge_Bat(p))
            elecdemandExpr.AddTerm(1, ElecSell(p))
            cpl.AddEq(elecgenExpr, cpl.Sum(Demand(0)(p), elecdemandExpr))

            '  //Heat generated must be greater than or equal heat demand
            Dim heatgenExpr As ILinearNumExpr = cpl.LinearNumExpr()
            Dim heatdemandExpr As ILinearNumExpr = cpl.LinearNumExpr()
            For t = 0 To Tech.Length - 1
                heatgenExpr.AddTerm(1, Gen(p)(t)(1))
            Next
            heatgenExpr.AddTerm(deff_TES, Discharge_TES(p))
            heatdemandExpr.AddTerm((1 / ceff_TES), Charge_TES(p))
            cpl.AddEq(heatgenExpr, cpl.Sum(Demand(1)(p), heatdemandExpr))
        Next

        For p = 0 To Period - 1
            '  //SELLING and BUYING ELECTRICITY 
            cpl.AddLe(ElecPur(p), cpl.Prod(M1, y1(p)))
            cpl.AddLe(ElecSell(p), cpl.Prod(M1, cpl.Sum(1, cpl.Prod(-1, y1(p)))))

            For t = 0 To Tech.Length - 1
                If Appl(t) = 5 Then
                    '  //CAPACITY CONSTRAINTS AND TECHNOLOGY MODELS
                    '  //CHP
                    Dim minchpExpr As ILinearNumExpr = cpl.LinearNumExpr()
                    Dim maxchpExpr As ILinearNumExpr = cpl.LinearNumExpr()
                    minchpExpr.AddTerm((MaxCap(t) * Suit(t)(0) * MinLoad(t)), Operate(p)(t))
                    maxchpExpr.AddTerm((MaxCap(t) * Suit(t)(0)), Operate(p)(t))
                    cpl.AddGe(Gen(p)(t)(0), minchpExpr)
                    cpl.AddLe(Gen(p)(t)(0), maxchpExpr)

                    '  //Heat Recovery from CHP units is less than or equal to electricity generation by CHP units times heat to power ratio of each unit
                    Dim heatrecovchpExpr As ILinearNumExpr = cpl.LinearNumExpr()
                    Dim elecgenhtpchpExpr As ILinearNumExpr = cpl.LinearNumExpr()
                    heatrecovchpExpr.AddTerm(1, Gen(p)(t)(1))
                    heatrecovchpExpr.AddTerm(1, dump(p)(t))
                    elecgenhtpchpExpr.AddTerm(htp(t), Gen(p)(t)(0))
                    cpl.AddEq(heatrecovchpExpr, elecgenhtpchpExpr)

                    '  //Limiting the amount of heat that chps can dump
                    cpl.AddLe(dump(p)(t), cpl.Prod(hdump, Gen(p)(t)(1)))    'HeatDump

                Else
                    '  //Limiting the amount of heat that chps can dump
                    cpl.AddEq(dump(p)(t), 0)                                'HeatDump2
                End If
            Next
        Next

        '  //BOILERS
        For p = 0 To Period - 1
            For t = 0 To Tech.Length - 1
                If Appl(t) = 3 Then
                    For u = 0 To EndUse - 1
                        Dim minbExpr As ILinearNumExpr = cpl.LinearNumExpr()
                        Dim maxbExpr As ILinearNumExpr = cpl.LinearNumExpr()
                        minbExpr.AddTerm((MaxCap(t) * Suit(t)(u) * MinLoad(t)), Operate(p)(t))
                        maxbExpr.AddTerm((MaxCap(t) * Suit(t)(u)), Operate(p)(t))
                        cpl.AddGe(Gen(p)(t)(u), minbExpr)
                        cpl.AddLe(Gen(p)(t)(u), maxbExpr)
                    Next
                End If
            Next
        Next

        '  //PV and Solar Thermal
        For p = 0 To Period - 1
            Dim solarareaExpr As ILinearNumExpr = cpl.LinearNumExpr()
            For t = 0 To Tech.Length - 1
                If Appl(t) = 11 Then
                    solarareaExpr.AddTerm(Area(t), Unit(t))
                    For u = 0 To EndUse - 1
                        Dim solarcapExpr As ILinearNumExpr = cpl.LinearNumExpr()
                        solarcapExpr.AddTerm((Area(t) * (Solar(p) / 1000) * _
                                              Efficiency(t) * OtherModEff * Suit(t)(u)), _
                                               Unit(t))
                        cpl.AddEq(Gen(p)(t)(u), solarcapExpr)
                    Next
                End If
            Next
            cpl.AddLe(solarareaExpr, MaxRoof)
        Next

        '  //HP
        For p = 0 To Period - 1
            For t = 0 To Tech.Length - 1
                If Appl(t) < 3 Then
                    For u = 0 To EndUse - 1
                        cpl.AddGe(Gen(p)(t)(u), cpl.Prod(Operate(p)(t), (MaxCap(t) * Suit(t)(u) * MinLoad(t))))
                        cpl.AddLe(Gen(p)(t)(u), cpl.Prod(Operate(p)(t), (MaxCap(t) * Suit(t)(u))))
                    Next
                End If
            Next
        Next

        '  //BATTERY MODEL
        For p = 1 To Period - 1
            Dim batstateExpr As ILinearNumExpr = cpl.LinearNumExpr()    'batstate
            batstateExpr.AddTerm((1 - decay), Stored_Bat(p - 1))
            batstateExpr.AddTerm(1, Charge_Bat(p))
            batstateExpr.AddTerm(-1, Discharge_Bat(p))
            cpl.AddEq(Stored_Bat(p), batstateExpr)
        Next

        cpl.AddEq(Stored_Bat(0), cpl.Prod(BatCap, min_state))           'initial state of battery

        For p = 0 To Period - 1
            cpl.AddGe(Stored_Bat(p), cpl.Prod(BatCap, min_state))       'min state of charge
            cpl.AddLe(Charge_Bat(p), cpl.Prod(BatCap, max_ch))          'battery charging
            cpl.AddLe(Discharge_Bat(p), cpl.Prod(BatCap, max_disch))    'battery discharging
        Next

        '  //TES Model
        For p = 1 To Period - 1
            Dim tesstateExpr As ILinearNumExpr = cpl.LinearNumExpr()    'tesstate
            tesstateExpr.AddTerm((1 - heatloss), Stored_TES(p - 1))
            tesstateExpr.AddTerm(1, Charge_TES(p))
            tesstateExpr.AddTerm(-1, Discharge_TES(p))
            cpl.AddEq(Stored_TES(p), tesstateExpr)
        Next
        cpl.AddEq(Stored_TES(0), Stored_TES(Period - 1))                'tesstate0
        cpl.AddEq(Discharge_TES(0), 0)                                  'tesdischarging0

        For p = 0 To Period - 1
            cpl.AddLe(Charge_TES(p), cpl.Prod(TESCap, maxch_TES))       'testcharging
            cpl.AddLe(Discharge_TES(p), cpl.Prod(TESCap, maxdisch_TES)) 'testdischarging
            cpl.AddLe(Stored_TES(p), TESCap)                            'testsizing
        Next

        '  //EMISSIONS TARGETS
        Dim totalemsExpr As ILinearNumExpr = cpl.LinearNumExpr()
        For p = 0 To Period - 1
            totalemsExpr.AddTerm(GridCO2EF, ElecPur(p))
            For t = 0 To Tech.Length - 1
                For u = 0 To EndUse - 1
                    If Tech(t) <> "CHP" Then
                        totalemsExpr.AddTerm((CO2EF(t) / Efficiency(t)), Gen(p)(t)(u))
                    ElseIf u = 0 And Tech(t) = "CHP" Then
                        totalemsExpr.AddTerm((CO2EF(t) / Efficiency(t)), Gen(p)(t)(u))
                    End If
                Next
            Next
        Next
        cpl.AddEq(CO2Emissions, totalemsExpr)

        Dim emlimit As Double = 0
        For p = 0 To Period - 1
            emlimit = emlimit + Demand(0)(p) * GridCO2EF * (1 - CO2Limit)
            emlimit = emlimit + (NGCO2EF * Demand(1)(p) / RefBoilerEff) * (1 - CO2Limit)
        Next
        cpl.AddLe(CO2Emissions, emlimit)


        'solve
        If cpl.Solve() Then
            System.Console.WriteLine(("Solution status = " + cpl.GetStatus().ToString))
            System.Console.WriteLine()
            System.Console.WriteLine(("Total Cost = " & cpl.ObjValue))

            'read result values
            System.Console.WriteLine()
            For t = 0 To Tech.Length - 1
                System.Console.WriteLine(Tech(t) + " capacity:" + ControlChars.Tab _
                                           + CStr(cpl.GetValue(Unit(t))))
            Next
        End If



        cpl.End()


    End Sub


    Friend Function OpenExcelGetData(ByVal fileNameAndPath As String, ByVal rowIndex As Integer, _
                                    ByVal columnIndex As String) As Double()
        Dim oExcelApp As New Excel.Application
        Dim oExcelBook As Excel.Workbook
        Dim oExcelSheet As Excel.Worksheet
        Dim sheetNumber As Integer = 1

        Dim oData(rowIndex - 1) As Double

        Try
            oExcelBook = oExcelApp.Workbooks.Open(fileNameAndPath)
            oExcelSheet = CType(oExcelBook.Worksheets(sheetNumber), Excel.Worksheet)

            'Read data
            Dim excelRange As String = columnIndex & rowIndex.ToString()
            For i As Integer = 1 To rowIndex
                excelRange = columnIndex & i.ToString()
                oData(i - 1) = oExcelSheet.Range(excelRange).Value
            Next

            oExcelApp.Workbooks.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Return oData
    End Function

End Class

